Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: CenterDevice JavaFxSVG
Source: https://github.com/codecentric/javafxsvg

Files: *
Copyright: 2017, CenterDevice GmbH
License: BSD-3

Files: src/main/java/de/codecentric/centerdevice/javafxsvg/BufferedImageTranscoder.java
Copyright: 2013, bb-generation
           2017, CenterDevice GmbH
License: BSD-3
Comment: This license for this file is documented as follows:
 /*
  * Code based on: https://gist.github.com/ComFreek/b0684ac324c815232556
  *
  * Many thanks to bb-generation for sharing this code!
  *
  * License unfortunately unknown, but using this code is probably categorized as
  * "fair use" (because the code is in my opinion too simple to be licensed)
  *
  * @author bb-generation
  * @see <a href="https://web.archive.org/web/20131215231214/http://bbgen.net/blog/2011/06/java-svg-to-bufferedimage/">java-svg-to-bufferedimage (as archived by archive.org)</a>
  */

Files: debian/*
Copyright: 2017, tony mancill <tmancill@debian.org>
License: BSD-3

License: BSD-3
 Copyright (c) 2015, codecentric AG
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the codecentric AG nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL CODECENTRIC AG BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
